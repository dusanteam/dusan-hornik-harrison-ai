const chalk = require('chalk');
const cp = require('child_process');
const fs = require('fs');

module.exports = async () => {
    
  try {
    console.log(chalk.cyan('Creating test data base...'));
    cp.spawnSync('npx', ['sequelize', 'db:create'], { stdio: 'inherit' });
    console.log(chalk.cyan('Migrating test data base...'));
    cp.spawnSync('npx', ['sequelize', 'db:migrate'], { stdio: 'inherit' });
    console.log(chalk.cyan('Creating seed data (users)...'));
    cp.spawnSync('npx', ['sequelize', 'db:seed:all'], { stdio: 'inherit' });

    const uploadPath = require('../server/models/index').config.uploadPath;
    if (!fs.existsSync(uploadPath)){
      console.log('Creating directory to upload photos ', uploadPath);
      fs.mkdirSync(uploadPath);
    }
    
  } catch (error) {
    console.log(chalk.red(error));
    process.exit(-1);
  }
};
