const userModel = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User', {
      name: DataTypes.STRING,
    },
    {},
  );
  User.associate = function(models) {
  };
  return User;
};

export default userModel;
