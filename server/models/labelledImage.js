const labelledImageModel = (sequelize, DataTypes) => {
  const LabelledImage = sequelize.define(
    'LabelledImage',
    {
      disease: DataTypes.STRING,
      area: DataTypes.JSONB,
      rawImageId: DataTypes.INTEGER,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {},
  );
  LabelledImage.associate = function() {
    // associations can be defined here
    // TODO has one RawImage
  };
  return LabelledImage;
};

export default labelledImageModel;
      