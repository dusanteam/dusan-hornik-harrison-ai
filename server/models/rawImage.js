const rawImageModel = (sequelize, DataTypes) => {
  const RawImage = sequelize.define(
    'RawImage',
    {
      name: DataTypes.STRING,
      path: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {},
  );
  RawImage.associate = function() {
    // associations can be defined here
    // TODO has many LabelledImages

  };
  return RawImage;
};

export default rawImageModel;
