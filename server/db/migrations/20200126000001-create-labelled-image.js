const migration = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('LabelledImages', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      rawImageId : {
        type: Sequelize.INTEGER,
        references: {
          model: 'RawImages',
          key: 'id',
        },
      },
      disease: {
        type: Sequelize.STRING,
      },
      area: {
        type: Sequelize.JSONB,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      createdBy : {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedBy : {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id',
        },
      },
    }),
  down: queryInterface => queryInterface.dropTable('LabelledImages'),
};

export default migration;
