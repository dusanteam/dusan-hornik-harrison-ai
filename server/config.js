module.exports = {
  development: {
    username: 'postgres',
    password: 'password',
    database: 'database_development',
    host: '127.0.0.1',
    dialect: 'postgres',
    jwtSecret: 'mySecret',
    uploadPath: 'data/uploads/',
  },
  test: {
    username: 'postgres',
    password: 'password',
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'postgres',
    jwtSecret: 'mySecret',
    uploadPath: 'data/test-uploads/',
  },
  production: {
    username: 'postgres',
    password: 'password',
    database: 'database_production',
    host: '127.0.0.1',
    dialect: 'postgres',
    jwtSecret: 'mySecret',
    uploadPath: 'data/uploads/',
  },
};
