const chalk = require('chalk');
const cp = require('child_process');
const rimraf = require("rimraf");

module.exports = async () => {
  console.log(chalk.cyan('Dropping test data base...'));
  cp.spawnSync('npx', ['sequelize', 'db:drop'], { stdio: 'inherit' });

  const uploadPath = require('../server/models/index').config.uploadPath;
  rimraf(uploadPath, function () { 
    console.log(chalk.cyan("Upload directory has been deleted")); 
  });
};
