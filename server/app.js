import express from 'express';
import { config } from './models';
import { resolve } from 'path';
import rawImagesRouter from './controllers/rawImages';
import labelledImagesRouter from './controllers/labelledImages';
import authRouter, { validateJwtToken, handleAuthorizationErrors } from './controllers/auth';
import cookieParser from 'cookie-parser';

const app = express();

global.appRoot = __dirname; 

app.use(cookieParser())                                                 // used to parse jwtToken cookie
app.use('/auth', authRouter);                                           // handles login and logout
app.use('/api', validateJwtToken);                                      // validate jwtToken on /api requests
app.use(handleAuthorizationErrors);               

app.use('/api/raw-images', rawImagesRouter);                            // api for raw images
app.use('/api/labelled-images', labelledImagesRouter);                  // api for labelled images

app.use('/uploads', validateJwtToken);                                  // validate jwtToken when serving uploaded images
app.use('/uploads', express.static(resolve('.', config.uploadPath)));   // serving raw images


app.use(express.static(resolve('..', 'build')));
app.all('*', (_, response) => {
  response.sendFile(resolve('..', 'build', 'index.html'));
});
app.use(  (err, req, res, next) => {
  console.error(err.stack)
  next(err)
});

export default app;
