import request from 'supertest';
import app from '../app';
import db from '../models';

const TEST_FILE_PATH = 'server/test/cat1.jpg';

afterAll(() => {
    db.sequelize.close()
});

describe('Auth endpoints tests', () => {

  it('login test invalid user', async done => {    
    await request(app)
      .post('/auth/login')
      .send({username:'invalid user', password:'password'})
      .expect(403)
    done()
  });

  it('login test invalid password', async done => {    
    await request(app)
      .post('/auth/login')
      .send({username:'John Doe', password:'invalid'})
      .expect(403)
    done()
  });

  it('login test correct details', async done => {    
    await request(app)
      .post('/auth/login')
      .send({username:'John Doe', password:'password'})
      .expect(200)
    done()
  });

  it('test logout', async done => {    
    await request(app)
      .post('/auth/logout')
      .expect('set-cookie', 'jwtToken=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT')
      .expect(200)

    done()
  });

  it('test Unathorised access to /api', async done => {    
    await request(app)
      .post('/api/raw-images')
      .expect(401)
    done()
  });

  it('test authorised access to /api', async done => {    
    const response = await request(app)
      .post('/auth/login')
      .send({username:'John Doe', password:'password'})
      .expect(200)
    const jwtToken = response.body.jwtToken

    await request(app)
      .get('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200)
    done()
  });

  it('test createBy field set correctly ', async done => {    
    const responseJohnDoe = await request(app)
      .post('/auth/login')
      .send({username:'John Doe', password:'password'})
      .expect(200)
    const jwtTokenJohnDoe = responseJohnDoe.body.jwtToken

    const responseFredJohnson = await request(app)
      .post('/auth/login')
      .send({username:'Fred Johnson', password:'password'})
      .expect(200)
    const jwtTokenFredJohnson = responseFredJohnson.body.jwtToken

    const createdByJohnDoe = await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtTokenJohnDoe])
      .field('name', 'John Doe cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201)

    const createdByFredJohnson = await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtTokenFredJohnson])
      .field('name', 'Fred Johnson cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201)

    expect(createdByJohnDoe.body.createdBy).toEqual(1)     // id of the user John Doe
    expect(createdByFredJohnson.body.createdBy).toEqual(2) // id of the user Fred Johnson
      
    done()
  });


});
