import request from 'supertest';
import app from '../app';
import db from '../models';
import jwt from 'jsonwebtoken';
import {config} from '../models';

const TEST_FILE_PATH = 'server/test/cat1.jpg';
const TEST_FILE_SIZE = "11094";
const jwtToken = jwt.sign({ id: 1, username: 'John' }, config.jwtSecret, { expiresIn: '1h' });

afterAll(() => {
  db.sequelize.close();
});

describe('RawImages endpoints tests', () => {

  it('POST (2x) then GET ALL', async done => {
    // upload two raw images
    await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .field('name', 'fake cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201);

    await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .field('name', 'real cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201);
    
    // GET all and assert result
    await request(app)
      .get('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(async res => {
        // find the cats
        const newRawImages = res.body
          .filter(rawImage=>['fake cat', 'real cat'].includes(rawImage.name))

        expect(newRawImages.length).toBe(2);

        await request(app)
          .delete(`/api/raw-images/${newRawImages[0].id}`)
          .set('Cookie', ['jwtToken=' + jwtToken])
          .expect(200);
        await request(app)
          .delete(`/api/raw-images/${newRawImages[1].id}`)
          .set('Cookie', ['jwtToken=' + jwtToken])
          .expect(200);
      })

    done();
  });

  it('POST then GET', async done => {
    
    // upload raw image
    const response = await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .field('name', 'semi-fake cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201);

    const imageId = response.body.id;
    const imagePath = response.body.path;

    // Assert the image entry is in database
    await request(app)
      .get(`/api/raw-images/${imageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect('Content-Type', /json/)
      .expect(200)
      .expect(res => expect(res.body.name).toBe('semi-fake cat'));

    // Assert the image file is served from filesystem
    await request(app)
      .get(`/uploads/${imagePath}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect('Content-Length', TEST_FILE_SIZE)
      .expect(200);

    await request(app)
      .delete(`/api/raw-images/${imageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200);

      done();
  });

  it('testing DELETE', async done => {
    
    // upload raw image
    const response = await request(app)
      .post('/api/raw-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .field('name', 'fake cat')
      .attach('file', TEST_FILE_PATH)
      .expect(201);

    const imageId = response.body.id;
    const imagePath = response.body.path;

    // delete image
    await request(app)
     .delete(`/api/raw-images/${imageId}`)
     .set('Cookie', ['jwtToken=' + jwtToken])
     .expect(200);

    // Assert image entry has been deleted from database
    await request(app)
      .get(`/api/raw-images/${imageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(404);
  
    // check the image has been deleted from filesystem
    await request(app)
     .get(`/uploads/${imagePath}`)
     .set('Cookie', ['jwtToken=' + jwtToken])
     .expect(404);

    done();
  });

});