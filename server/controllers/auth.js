import db, {config} from '../models';
import { Router } from 'express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';

// ideally I would use double submit cookie method https://stackoverflow.com/a/29622103 
// but for this assignment storing jwt in standard cookie should be fine

export const validateJwtToken = expressJwt({ 
  secret: config.jwtSecret,
  getToken: (req) => {
    return req.cookies.jwtToken;
  }
})

export const handleAuthorizationErrors = (err, req, res, next) => {  
  if (err.name === 'UnauthorizedError') {    
    res.status(401).json({"error" : err.name + ": " + err.message})  
  }
}

const router = Router();

router.all('/logout', async (req, res) => {
  res.clearCookie('jwtToken');
  res.status(200).send();
});

router.post('/login', bodyParser.json(), async (req, res) => {
  try {
    const username = req.body.username;
    const password = req.body.password;

    const user = await doLogin(username, password);
    if (user) {
      const jwtToken = jwt.sign({ id: user.id, username: user.name }, config.jwtSecret, { expiresIn: '1h' });

      res.cookie('jwtToken', jwtToken, { maxAge: 1000 * 60 * 60, /* 1h */ });
      return res.json({
        success: true,
        message: 'Authentication successful!',
        jwtToken: jwtToken
      });
    } 
    return res.status(403).json({
      success: false,
      message: 'Incorrect username or password'
    });
} catch (error) {
    return res.status(res.statusCode).json(error);
  }
});

const doLogin = async (username, password) => {
  // Real app will call real Auth api, 
  // here we just pretend that all users have password 'password'
  // Alternatively we could store encrypted passwords in Users table
  if (password !== 'password') {
    return null;
  }
  return await db.User.findOne({ where: { name: username } });
}

export default router;