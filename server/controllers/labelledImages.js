import db from '../models';
import { Router } from 'express';
import bodyParser from 'body-parser';

const router = Router();

router.get('/', async (_, response) => {
  const labelledImages = await db.LabelledImage.findAll();
  return response.status(200).json(labelledImages);
});

router.get('/search-by-disease/:disease', async (request, response) => {
  const labelledImages = await db.LabelledImage.findAll({ where: { disease: request.params.disease } });
  return response.status(200).json(labelledImages);
});

router.get('/:id', async (request, response) => {
  const labelledImage = await db.LabelledImage.findByPk(request.params.id);
  if (labelledImage !== null) {
    return response.status(200).json(labelledImage);
  }
  return response.status(404).send();
});

router.post('/', bodyParser.json(), async (request, response) => {
  try {
    const createdBy = request.user.id;
    const labelledImage = await db.LabelledImage.create({ ...request.body, createdBy });
    return response.status(201).json(labelledImage);
  } catch (error) {
    return response.status(response.statusCode).json(error);
  }
});

router.put('/:id', bodyParser.json(), async (request, response) => {
  try {
    const updatedBy = request.user.id;
    const updated = await db.LabelledImage.update(
      { ...request.body, updatedBy }, {
      returning: true,
      fields: ['area', 'disease', 'updatedBy'],
      where: { id: request.params.id }
    });
    return response.status(200).json(updated[1][0]);
  } catch (error) {
    return response.status(response.statusCode).json(error);
  }
});

router.delete('/:id', async (request, response) => {
  // delete all associated labelled images
  await db.LabelledImage.destroy({
    where: {
      id: request.params.id
    }
  });
  return response.status(200).send();
});

export default router;
