import db, {config} from '../models';
import { Router } from 'express';
import multer from 'multer';
import fs from 'fs';

const upload = multer({ dest: '/tmp/' });

const router = Router();

// There is no PUT request as changed raw image would invalidate labelled images
/** Get all images */
router.get('/', async (_, response) => {
  const rawImages = await db.RawImage.findAll();
  return response.status(200).json(rawImages);
});

/** Get one image */
router.get('/:id', async (request, response) => {
  const rawImage = await db.RawImage.findByPk(request.params.id);
  if (rawImage !== null) {
    return response.status(200).json(rawImage);
  }
  return response.status(404).send();
});

/** Delete one image */
router.delete('/:id', async (request, response) => {
  // delete all associated labelled images (todo look into sequelizer cascade deletes)
  await db.LabelledImage.destroy({ where: { rawImageId: request.params.id } });
  
  // delete file from filesystem
  const rawImage = await db.RawImage.findByPk(request.params.id);
  fs.unlinkSync(getAbsoluteFilePath(rawImage.path));

  // delete actual RawImage model from database
  await db.RawImage.destroy({ where: { id: request.params.id } });
  
  return response.status(200).send();
});

/** Upload one image */
router.post('/', upload.single('file'), (request, response) => {
  const createdBy = request.user.id;
  const file = getAbsoluteFilePath(request.file.filename);

  fs.rename(request.file.path, file, function (err) {
    if (err) {
      console.log(err);
      response.send(500);
    } else {
      db.RawImage.create({
        name: request.body.name,
        description: request.body.description,
        path: request.file.filename,
        createdBy,
      }).then(r => {
        response.status(201).send(r.get({ plain: true }));
      });
    }
  });
});

export default router;

const getAbsoluteFilePath = (filename) => {
  return global.appRoot + '/../' + config.uploadPath + filename;
}
