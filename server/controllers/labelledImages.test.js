import request from 'supertest';
import app from '../app';
import db from '../models';
import jwt from 'jsonwebtoken';
import {config} from '../models';

const TEST_FILE_PATH = 'server/test/cat1.jpg';
const jwtToken = jwt.sign({ id: 1, username: 'John' }, config.jwtSecret, { expiresIn: '1h' });

afterAll(() => {
    db.sequelize.close();
});

describe('LabelledImages endpoints tests', () => {

  it('GET ALL - empty', async done => {    
    const rawImageId = await createRawImage();

    await request(app)
      .get('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200)
      .expect(res=>expect(res.body.length).toBe(0));

    await deleteRawImage(rawImageId);
    done();
  });

  it('POST then GET', async done => {    
    const rawImageId = await createRawImage();
    const response = await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus', area:[]})
      .expect(201)
    
    const labelledImageId = response.body.id;

    await request(app)
      .get(`/api/labelled-images/${labelledImageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200)
      .expect(res=>{
        expect(res.body.disease).toBe('virus')
        expect(res.body.area).toEqual([])
        expect(res.body.rawImageId).toBe(rawImageId)
      })
      .then(async () => 
        await request(app)
          .delete(`/api/labelled-images/${labelledImageId}`)
          .set('Cookie', ['jwtToken=' + jwtToken])
          .expect(200)
      )
    await deleteRawImage(rawImageId);
    done();
  });

  it('POST then PUT then GET', async done => {    
    const rawImageId = await createRawImage();
    const response = await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus', area:[]})
      .expect(201)
    
    const labelledImageId = response.body.id;

    await request(app)
      .put(`/api/labelled-images/${labelledImageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'bad virus', area:[{a:2}]})
      .expect(200)

    await request(app)
      .get(`/api/labelled-images/${labelledImageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200)
      .expect(res=>{
        expect(res.body.disease).toBe('bad virus')
        expect(res.body.area).toEqual([{a:2}])
        expect(res.body.rawImageId).toBe(rawImageId)
      })
      .then(async () => 
        await request(app)
          .delete(`/api/labelled-images/${labelledImageId}`)
          .set('Cookie', ['jwtToken=' + jwtToken])
          .expect(200)
      )
    await deleteRawImage(rawImageId);
    done();
  });

  it('test POST then DELETE', async done => {    
    // create
    const rawImageId = await createRawImage();
    const response = await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus', area:[]})
      .expect(201)
    
    const labelledImageId = response.body.id;

    // delete
    await request(app)
      .delete(`/api/labelled-images/${labelledImageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(200)

    // assert it's gone
    await request(app)
      .get(`/api/labelled-images/${labelledImageId}`)
      .set('Cookie', ['jwtToken=' + jwtToken])
      .expect(404);
    await deleteRawImage(rawImageId);
    done();
  });


  it('search by disease', async done => {    
    // create
    const rawImageId = await createRawImage();

    await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus1', area:[]})
      .expect(201)
    
    await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus2', area:[]})
      .expect(201)
    
    await request(app)
      .post('/api/labelled-images')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus1', area:[]})
      .expect(201)

    // search virus1
    const response1 = await request(app)
      .get('/api/labelled-images/search-by-disease/virus1')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus1', area:[]})
      .expect(200)
    expect(response1.body.length).toBe(2)

    // search virus2
    const response2 = await request(app)
      .get('/api/labelled-images/search-by-disease/virus1')
      .set('Cookie', ['jwtToken=' + jwtToken])
      .send({rawImageId, disease:'virus2', area:[]})
      .expect(200)
    expect(response2.body.length).toBe(2)

    await deleteRawImage(rawImageId);
    done();
  });




});

const createRawImage = async () => {
  const response = await request(app)
    .post('/api/raw-images')
    .set('Cookie', ['jwtToken=' + jwtToken])
    .field('name', 'semi-real cat')
    .attach('file', TEST_FILE_PATH)
    .expect(201);
  return response.body.id;
}

const deleteRawImage = async (id) => {
  await request(app)
    .delete(`/api/raw-images/${id}`)
    .set('Cookie', ['jwtToken=' + jwtToken])
    .expect(200);
}
