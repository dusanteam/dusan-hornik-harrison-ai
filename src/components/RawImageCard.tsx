import React from 'react';
import { RawImage, LabelledImage } from './types';

type RawImageCardProps = {
  rawImage: RawImage;
  labelledImages: LabelledImage[];
  onDelete: () => void;
  onAddLabelledImage: (labelledImage: LabelledImage) => void;
  onLabelledImageSelect: (labelledImage: LabelledImage) => void;
}

const RawImageCard: React.FC<RawImageCardProps> = (props) => {

  const deleteRawImage = () => {
    fetch(`/api/raw-images/${props.rawImage.id}`, { method: 'DELETE' })
      .then(props.onDelete)
  }

  const addLabelledImage = () => {
    const data= { rawImageId: props.rawImage.id, area: [] };
    const asString = JSON.stringify(data);
    console.log('Data', data);
    console.log('asString', asString);
    fetch(`/api/labelled-images`, {
      method: 'POST',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: asString
    })
      .then(res => res.json())
      .then(props.onAddLabelledImage)      
  }

  return (
    <div key={props.rawImage.id} className="card">
      <div className="card-body">
        <button className="btn-outline-danger btn btn-sm float-right" onClick={deleteRawImage}>Delete</button>

        <h6>Raw image {props.rawImage.id}: {props.rawImage.name} </h6>
        <img src={`/uploads/${props.rawImage.path}`} width="330px" alt={props.rawImage.name}></img>

        <button className="btn-outline-primary btn btn-sm mt-2" onClick={addLabelledImage}>Add labelled image</button>

        <h6 className="mt-3">Labelled images:</h6>
        {props.labelledImages.map(labelledImage =>
            <div key={labelledImage.id}>
              <button className="btn btn-link btn-sm" onClick={() => props.onLabelledImageSelect(labelledImage) }>{labelledImage.id} {labelledImage.disease}</button>
            </div>
          )}
      </div>
    </div>)
}
export default RawImageCard;