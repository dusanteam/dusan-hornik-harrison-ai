import React, { useState, useEffect } from 'react';
import { RawImage, LabelledImage } from './types';
import { ReactPictureAnnotation } from 'react-picture-annotation';

type ImageAnnotatorProps = {
  labelledImage: LabelledImage;
  rawImage: RawImage
  onSave: () => void;
  onDelete: () => void;
}

const ImageAnnotator: React.FC<ImageAnnotatorProps> = (props) => {
  const [disease, setDisease] = useState<string>(props.labelledImage.disease || '');
  const [area, setArea] = useState<any>(props.labelledImage.area || []);

  useEffect(() => {
    setDisease(props.labelledImage.disease || '')
    setArea(props.labelledImage.area || [])
  }, [props.labelledImage]);


  const saveChanges = () => {
    fetch(`/api/labelled-images/${props.labelledImage.id}`, {
      method: 'PUT',
      headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
      body: JSON.stringify({ ...props.labelledImage, disease, area })
    }).then(() => props.onSave())
  }

  const deleteLabelledImage = () => {
    fetch(`/api/labelled-images/${props.labelledImage.id}`, { method: 'DELETE' })
      .then(() => props.onDelete())
  }

  return (
    <>
      <button className="btn-outline-primary btn btn-sm float-right" onClick={saveChanges}>Save changes</button>
      <button className="btn-outline-danger btn btn-sm float-right" onClick={deleteLabelledImage}>Delete</button>

      <h6>Labelled Image</h6>
      <hr></hr>
      <h3>{props.labelledImage.id} {disease}</h3>
      <form>
        <div className="form-group row">
          <label className="col-2 col-form-label" htmlFor="diseaseInput">Disease:</label>
          <div className="col-10">
            <input 
              type="text" 
              className="form-control" 
              id="diseaseInput" 
              name="disease" 
              placeholder="Enter disease name"
              value={disease}
              onChange={e => setDisease(e.target.value)}
            />
          </div>
        </div>
      </form>
      <label>Drag rectangle(s) on the picture:</label>
      <ReactPictureAnnotation
        image={`/uploads/${props.rawImage.path}`}
        onSelect={()=>{}}
        onChange={setArea}
        width={500}
        height={300}
        annotationData={area}
      />
    </>
  )
}
export default ImageAnnotator;