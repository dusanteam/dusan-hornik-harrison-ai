import React, { useState } from 'react';

type FileUploadProps = {
  onUpload: () => void;
}

const FileUpload: React.FC<FileUploadProps> = (props) => {
  const [file, setFile] = useState<File>();
  
  const fileLabel = (file?: File): React.ReactNode => {
    if (file) {
      return `${file.name} (${file.size})`;
    }
  }
  const handleFileSelectionChange = (event: React.FormEvent<HTMLInputElement>) => {
    const input = event.currentTarget;
    if (!input.files) {
      // no file selected
      return;
    }
    setFile(input.files[0]);
    // reset the value of input box so onchange event is fired even 
    // when user selects same file again https://stackoverflow.com/a/28274454
    input.value = '';
  }

  const doUploadFile = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.preventDefault();
    if (!file) {
      console.log('No file to upload has been selected');
      return;
    }
    var data = new FormData();
    data.append('file', file);
    data.append('name', file.name);

    fetch('/api/raw-images', { method: 'POST', body: data })
      .then(() => setFile(undefined))
      .then(props.onUpload);
  }

  return (
    <form>
    <div className="form-row">
      <div className="col-md-12">
        <div>
          <input type="file" accept="image/*" className="custom-file-input" id="fileSelect" onChange={handleFileSelectionChange} />
          <label className="custom-file-label" htmlFor="fileSelect">{fileLabel(file) || 'Select raw image file'}</label>
        </div>
      </div>
    </div>

    <div className="form-row mt-4">
      <div className="col-md-9">
        <button className="btn btn-primary" type="submit" onClick={doUploadFile} disabled={file===undefined}>
          Upload
        </button>
      </div>
    </div>
  </form>  )
}
export default FileUpload;