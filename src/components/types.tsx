export type User = {
  id: number;
  name: string;
}

export type RawImage = {
  id: number;
  name: string;
  path: string;
}

// todo share types with server
export type LabelledImage = {
  id: number;
  rawImageId: number;
  name: string;
  area: any;
  disease: string;
}