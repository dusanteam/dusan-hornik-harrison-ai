import React from 'react';

type LoginLogoutButtonsProps = {
  jwtToken: string;
  onLogin: () => void;
  onLogout: () => void;
}

// TODO: implement as proper Login form
const LoginLogoutButtons: React.FC<LoginLogoutButtonsProps> = (props) => {

  const renderLoginButton = () => {
    return (
      <button className="btn float-right btn-primary" onClick={() => {
        alert('You will be logged in as "Fred Johnson" with password "password".');
        fetch(`/auth/login`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' },
          body: JSON.stringify({ username: 'Fred Johnson', password: 'password' }),
        })
          .finally(props.onLogout)
      }}>
        Login
      </button>
    )
  }

  const renderLogoutButton = () => {
    return (
      <button className="btn float-right btn-primary" onClick={() => fetch(`/auth/logout`).finally(props.onLogout)}>
        Logout
      </button>
    )
  }

  return (
    <>
      {!props.jwtToken && renderLoginButton()}
      {props.jwtToken && renderLogoutButton()}
    </>
  )
}
export default LoginLogoutButtons;