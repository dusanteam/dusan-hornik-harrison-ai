import React, { useState, useEffect } from 'react';
import './App.css';
import { RawImage, LabelledImage } from './components/types';
import Cookies from 'js-cookie';
import ImageAnnotator from './components/ImageAnnotator';
import FileUpload from './components/FileUpload';
import RawImageCard from './components/RawImageCard';
import LoginLogoutButtons from './components/LoginLogoutButtons';

const App: React.FC = () => {
  const [selectedLabelledImage, setSelectedLabelledImage] = useState<LabelledImage>();
  const [rawImages, setRawImages] = useState<RawImage[]>();
  const [labelledImages, setLabelledImages] = useState<LabelledImage[]>();
  const [jwtToken, setJwtToken] = useState<any>();

  useEffect(() => {
    reloadData();
  },[]);

  const reloadData = async () => {
    try {
      setSelectedLabelledImage(undefined);
      readJwtTokenFromCookie();
      await getRawImages();
      await getLabelledImages();
      } catch (e) {
        console.error('Error re-loading data', e)
        setRawImages(undefined);
        setLabelledImages(undefined)    
      }
  };

  // ------------ fetch data ---------------
  const getRawImages = async () => {
    const res = await fetch('/api/raw-images');
    if (!res.ok) {
      throw Error(`Request rejected with status ${res.status}`);
    }
    const json = await res.json();
    setRawImages(json.reverse());
  }

  const getLabelledImages = async () => {
    const res = await fetch('/api/labelled-images');
    if (!res.ok) {
      throw Error(`Request rejected with status ${res.status}`);
    }
    const json = await res.json();
    setLabelledImages(json.reverse());
  }

  const readJwtTokenFromCookie = () => { 
    setJwtToken(Cookies.get('jwtToken'));
  }

  // ------------ data utilities ---------------
  const getLabelledImagesForRawImage = (rawImageId: number) => {
    if (!labelledImages) {
      return [];
    }
    return labelledImages.filter(labelledImage => labelledImage.rawImageId === rawImageId);
  }

  const getRawImageById = (id: number) => {
    if (!rawImages) {
      throw new Error('RawImage ' + id + ' not found');
    }
    const rawImage = rawImages.find(rawImage => rawImage.id === id);
    if (!rawImage) {
      throw new Error('RawImage ' + id + ' not found');
    }
    return rawImage;
  }

  return (
    <div className="container-fluid">
      <div className="welcome">

        <LoginLogoutButtons
          jwtToken={jwtToken}
          onLogin={reloadData}
          onLogout={reloadData}
        />

        <h1>Hello Harrison AI</h1>

        {!jwtToken && <p className="lead">To start you need to log in</p>}
        {jwtToken && <p className="lead">Upload new raw image and start labelling</p>}
      </div>

      <div className="row">
        <div className="col-1 min-width">

          {jwtToken && <div className="card">
            <div className="card-body">
              <FileUpload onUpload={getRawImages}/>
            </div>
          </div>}

          {rawImages &&
            rawImages.map(rawImage =>
              <RawImageCard 
                key={rawImage.id}
                rawImage={rawImage}
                labelledImages={getLabelledImagesForRawImage(rawImage.id)}
                onAddLabelledImage={labelledImage => { getLabelledImages(); setSelectedLabelledImage(labelledImage); }}
                onDelete={() => { getRawImages(); setSelectedLabelledImage(undefined) }}
                onLabelledImageSelect={setSelectedLabelledImage}
              />)
            }
        </div>
        <div className="col">
          {selectedLabelledImage &&
            <ImageAnnotator 
              labelledImage={selectedLabelledImage}
              rawImage={getRawImageById(selectedLabelledImage.rawImageId)}
              onDelete={()=>{setSelectedLabelledImage(undefined);getLabelledImages()}}
              onSave={getLabelledImages}
            />
          }
        </div>

      </div>
    </div>
  );
};

export default App;
