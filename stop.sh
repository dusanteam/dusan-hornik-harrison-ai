docker-compose -f postgres-stack.yml down     # stop postgres
pkill node                                    # kill node
pkill docker-compose                          # kill log tail (started by start.sh)