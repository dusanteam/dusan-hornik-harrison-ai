docker-compose -f postgres-stack.yml up -d   # start postgres container

# attempt to wait until database starts
docker-compose -f postgres-stack.yml logs -f db &
( docker-compose -f postgres-stack.yml logs -f db & ) | grep -q "database system is ready to accept connections"

yarn                                         # download dependencies            

mkdir data/uploads                           # directory for uploaded images
npx sequelize db:create                      # create database
npx sequelize-cli db:migrate                 # create tables
npx sequelize-cli db:seed:all                # populate with seed data (2 users)

yarn start-server & yarn start-client        # start node server and react client