# Harrison AI Coding Test
Implemented by Dusan Hornik

## Overview

- Node backend 
- React frontend 
- Postgres database

## Instructions to start

#### TLDR 
```
./initAndStart.sh   # start postgres, create DB, start node server, start react client
./stop.sh           # stop postgres, kill node, kill client
./start.sh          # start postgres, start node server, start react client
```
#### Instructions to start - details

Requirements: docker, node, npx, yarn

Start postgres 
```
docker-compose -f postgres-stack.yml up
```

Create the database:
```
mkdir data/uploads              # directory for uploaded images
npx sequelize db:create         # create database
npx sequelize-cli db:migrate    # create tables
npx sequelize-cli db:seed:all   # populate with seed data (2 users)
```

Start node server
```
yarn start-server
```

Start UI client
```
yarn start-client
```

- Client url: http://localhost:3000/
- Server url: http://localhost:9000/
- Adminer url: http://localhost:8080  (System: PostgreSQL, Server: db, Username: postgres, Password: password)

## Implementation

Based on create-fullstack-react-app
```
npx create-fullstack-react-app dusan-hornik-harrison-ai
```

- Using Jest for tests
- Using sequilizer to persist / query data to PostgreSQL. 
- Using JWT for authentication

## Tests

Make sure database is running
```
docker-compose -f postgres-stack.yml up
```

Then run 
```
yarn test-server
```

### Folder structure

#### Server
 - /server - code for node application
 - to review server code start here: [server/app.js](server/app.js)

#### Client
 - /src /public - code for React application
 - to review React application start here: [src/App.tsx](src/App.tsx)

#### Data
 - /data/postgres - postgres data directory (see postgres-stack.yml)
 - /data/uploads - directory for uploaded images
 - /data/test-uploads - directory for images uploaded during test

## Requirements
#### 1. MUST have an API server written in JavaScript
Done. See [server/app.js](server/app.js)

#### 2. MUST have routes for raw medical images 
Done. See [server/app.js](server/app.js) and [server/controllers/rawImages.js](server/controllers/rawImages.js)

#### 3. MUST have routes for labelled images
Done. See [server/app.js](server/app.js) and [server/controllers/labelledImages.js](server/controllers/labelledImages.js)

#### 4. MUST persist data and/or metadata to a database 
Done. [server/controllers/rawImages.js](server/controllers/rawImages.js) and [server/controllers/labelledImages.js](server/controllers/labelledImages.js)

#### 5. MUST be secured against anonymous access 
Done using JWT. See [server/app.js](server/app.js) and [server/controllers/auth.js](server/controllers/auth.js)
  
#### 6. MUST contain tests using a testing framework
Done using Jest. See [server/controllers/](server/controllers/) 

#### 7. SHOULD be able to detect sensitive data Personal Identifiable Information) which is part of the image
Not done, requirements are not too clear. Does it involve reading exif data from the image and looking for name, DOB, etc? 

#### 8. SHOULD track which user makes changes to the data 
Done. Not a full audit log, but there are fields createdBy, updatedBy on RawImage and LabelledImage models

#### 9. COULD have a UI (but don’t worry about UX 
Done. React app to upload and add label on images. (No tests in the UI yet)

#### 10. COULD support searching for labelled medical images by Date, Disease type, or Status
Implemented search by disease endpoint that returns labelled images by given disease
```
router.get('/search-by-disease/:disease', async (request, response) => {
...
});
```

Search api is not used in UI yet


