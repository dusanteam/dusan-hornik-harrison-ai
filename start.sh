docker-compose -f postgres-stack.yml up -d   # start postgres container

# attempt to wait until database starts
docker-compose -f postgres-stack.yml logs -f db &
( docker-compose -f postgres-stack.yml logs -f db & ) | grep -q "database system is ready to accept connections"

yarn start-server & yarn start-client        # start node server and react client